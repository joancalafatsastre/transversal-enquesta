<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        if (0 === strpos($pathinfo, '/admin')) {
            // insertProduct
            if ('/admin/insertProduct' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\ProductController::insertProductAction',  '_route' => 'insertProduct',);
            }

            // selectAllProducts
            if ('/admin/selectAllProducts' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\ProductController::selectAllProductsAction',  '_route' => 'selectAllProducts',);
            }

            // updateProduct
            if ('/admin/updateProduct' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\ProductController::updateProductAction',  '_route' => 'updateProduct',);
            }

            // editProduct
            if (0 === strpos($pathinfo, '/admin/editProduct') && preg_match('#^/admin/editProduct/(?P<name>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'editProduct')), array (  '_controller' => 'AppBundle\\Controller\\ProductController::editProductAction',));
            }

            // removeProduct
            if ('/admin/removeProduct' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\ProductController::removeProductAction',  '_route' => 'removeProduct',);
            }

        }

        // homepage
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController::urlRedirectAction',  'path' => '/admin/selectAllProducts',  'permanent' => true,  '_route' => 'homepage',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_homepage;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'homepage'));
            }

            return $ret;
        }
        not_homepage:

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
