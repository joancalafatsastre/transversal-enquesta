<?php

/* product/content.html.twig */
class __TwigTemplate_13d31f6803ee82fd7a824ac0c8c4bb54b64522bc8964a9060ee39a02112c0066 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "product/content.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'bodys' => array($this, 'block_bodys'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"card-container\">
        ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 6
            echo "        <div class=\"card\">
            <div class=\"card-image\">
                <figure class=\"image is-4by3\">
                    <img src=\"https://bulma.io/images/placeholders/1280x960.png\" alt=\"Placeholder image\">
                </figure>
            </div>
            <div class=\"card-content\">
                <div class=\"media\">
                    <div class=\"media-left\">
                        <span class=\"icon\">
                    ";
            // line 16
            if (($this->getAttribute($context["product"], "destacada", array()) == 1)) {
                // line 17
                echo "                        <i class=\"fas fa-star\"></i>
                    ";
            } else {
                // line 19
                echo "                        <i class=\"far fa-star\"></i>
                    ";
            }
            // line 21
            echo "                        </span>

                    </div>
                    <div class=\"media-content\">
                        <p class=\"title is-4\">ID: ";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "</p>
                        <p class=\"subtitle is-6\">";
            // line 26
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["product"], "dataInici", array()), "Y-m-d"), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["product"], "dataFinal", array()), "Y-m-d"), "html", null, true);
            echo "</p>

                    </div>
                </div>

                <div class=\"content\">
                    <p><span class=\"has-text-weight-bold\">Pregunta: </span>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "pregunta", array()), "html", null, true);
            echo "</p>

<p>Vots si:<span class=\"resultatsi\"></span>
\t\t<div class=\"votssi\">

\t\t";
            // line 37
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["info"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["enquesta"]) {
                // line 38
                echo "\t\t\t\t
\t\t\t\t";
                // line 39
                if (($this->getAttribute($context["product"], "id", array()) == $this->getAttribute($context["enquesta"], "idEnquesta", array()))) {
                    // line 40
                    echo "\t\t\t\t    ";
                    if (($this->getAttribute($context["enquesta"], "valor", array()) == true)) {
                        // line 41
                        echo "<span>
\t\t\t\t\t\t";
                        // line 42
                        echo 0;
                        echo "
</span>
\t\t\t\t\t";
                    }
                    // line 45
                    echo "\t\t\t\t";
                }
                // line 46
                echo "\t\t\t\t
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enquesta'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo " 

\t\t</div>
\t\t
</p>


<p>Vots no: <span class=\"resultatno\"></span>
\t\t<div class=\"votsno\">

\t\t";
            // line 57
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["info"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["enquesta"]) {
                // line 58
                echo "\t\t\t\t
\t\t\t\t";
                // line 59
                if (($this->getAttribute($context["product"], "id", array()) == $this->getAttribute($context["enquesta"], "idEnquesta", array()))) {
                    // line 60
                    echo "\t\t\t\t    ";
                    if (($this->getAttribute($context["enquesta"], "valor", array()) == false)) {
                        // line 61
                        echo "\t\t\t\t\t<span>
\t\t\t\t\t\t";
                        // line 62
                        echo 0;
                        echo "
\t\t\t\t\t</span>
\t\t\t\t\t";
                    }
                    // line 65
                    echo "\t\t\t\t";
                }
                // line 66
                echo "
\t\t\t\t
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enquesta'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 68
            echo " 

\t\t</div>
\t\t
</p>

                </div>
            </div>
        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 78
        echo "
    </div>

";
    }

    // line 82
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 83
        echo "    <style>
        .card-container {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-around;
        }

        .card {
            margin: 1rem;
            width: 30%;
        }
        .media-content {
            overflow: hidden;
        }
    </style>
";
        // line 99
        $this->displayBlock('bodys', $context, $blocks);
    }

    public function block_bodys($context, array $blocks = array())
    {
        // line 100
        $this->displayBlock('javascripts', $context, $blocks);
    }

    public function block_javascripts($context, array $blocks = array())
    {
        // line 101
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-3.3.1.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t<script src=\"";
        // line 102
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/js.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "product/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 102,  221 => 101,  215 => 100,  209 => 99,  192 => 83,  189 => 82,  182 => 78,  167 => 68,  159 => 66,  156 => 65,  150 => 62,  147 => 61,  144 => 60,  142 => 59,  139 => 58,  135 => 57,  123 => 47,  116 => 46,  113 => 45,  107 => 42,  104 => 41,  101 => 40,  99 => 39,  96 => 38,  92 => 37,  84 => 32,  73 => 26,  69 => 25,  63 => 21,  59 => 19,  55 => 17,  53 => 16,  41 => 6,  37 => 5,  34 => 4,  31 => 3,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "product/content.html.twig", "/var/www/html/admin/app/Resources/views/product/content.html.twig");
    }
}
