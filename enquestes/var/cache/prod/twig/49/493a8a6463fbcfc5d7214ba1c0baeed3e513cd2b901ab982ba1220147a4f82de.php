<?php

/* login/login.html.twig */
class __TwigTemplate_572f77cd5c9a74f566fa7206fe917ff9b34e0e77d60e2213acbe1f16460ce97e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "login/login.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "    <style>

    </style>
";
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        // line 11
        echo "    <div class=\"container\">
        <form action=\"\" method=\"POST\">
            <label for=\"\">
                Nom d'usuari
                <input type=\"text\" name=\"username\" placeholder=\"Introdueix el teu nom d'usuari\">
            </label>
            <label for=\"\">
                Contrassenya
                <input type=\"password\" name=\"password\" placeholder=\"Introdueix la teva contrassenya\">
            </label>
        </form>
    </div>
";
    }

    // line 26
    public function block_javascripts($context, array $blocks = array())
    {
        // line 27
        echo "    <script>

    </script>
";
    }

    public function getTemplateName()
    {
        return "login/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 27,  59 => 26,  43 => 11,  40 => 10,  33 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "login/login.html.twig", "/var/www/html/admin/app/Resources/views/login/login.html.twig");
    }
}
