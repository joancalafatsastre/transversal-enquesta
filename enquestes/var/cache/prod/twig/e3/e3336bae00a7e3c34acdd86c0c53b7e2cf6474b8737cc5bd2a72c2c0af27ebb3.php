<?php

/* base.html.twig */
class __TwigTemplate_5e07e1787f96caba1e2b222af1001353614630255f028ad8175a20bc33286edb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'upperbar2' => array($this, 'block_upperbar2'),
            'body' => array($this, 'block_body'),
            'scripts' => array($this, 'block_scripts'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css\">
        <script defer src=\"https://use.fontawesome.com/releases/v5.0.6/js/all.js\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js\"></script>
        ";
        // line 10
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
    <header>
        <nav class=\"navbar is-dark\" role=\"navigation\" aria-label=\"main navigation\">
            <div class=\"navbar-brand\">
                <a class=\"navbar-item\" href=\"/\">
                    <!--<img src=\"/img/polljtwox.png\" alt=\"Bulma: a modern CSS framework based on Flexbox\" width=\"112\" height=\"128\">
                </a>-->



                <div class=\"navbar-burger\" data-target=\"mainMenu\">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class=\"navbar-menu\" id=\"mainMenu\">
                <div class=\"navbar-end\">
                    <a href=\"/admin/selectAllProducts\" class=\"navbar-item\">
                        Llistar enquestes
                    </a>
                    <a href=\"/admin/insertProduct\" class=\"navbar-item\">
                        Afegir enquestes
                    </a>
                    
                    </a>
                    <a href=\"/admin/updateProduct\" class=\"navbar-item\">
                        Modificar enquestes
                    </a>
                    <a href=\"/admin/removeProduct\" class=\"navbar-item\">
                        Esborrar enquestes
                    </a>
                </div>
            </div>
        </nav>
    </header>
       <!-- <h3>Customer Management</h3>
        <div id=\"upperbar2\">
            ";
        // line 50
        $this->displayBlock('upperbar2', $context, $blocks);
        // line 58
        echo "        </div>-->
        <div id=\"content\">
            ";
        // line 60
        $this->displayBlock('body', $context, $blocks);
        // line 61
        echo "        </div>
    <script>
        document.addEventListener('DOMContentLoaded', function () {

            // Get all \"navbar-burger\" elements
            var \$navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

            // Check if there are any navbar burgers
            if (\$navbarBurgers.length > 0) {

                // Add a click event on each of them
                \$navbarBurgers.forEach(function (\$el) {
                    \$el.addEventListener('click', function () {

                        // Get the target from the \"data-target\" attribute
                        var target = \$el.dataset.target;
                        var \$target = document.getElementById(target);

                        // Toggle the class on both the \"navbar-burger\" and the \"navbar-menu\"
                        \$el.classList.toggle('is-active');
                        \$target.classList.toggle('is-active');

                    });
                });
            }

        });
    </script>
        ";
        // line 89
        $this->displayBlock('scripts', $context, $blocks);
        // line 90
        $this->displayBlock('javascripts', $context, $blocks);
        // line 94
        echo "    </body>
</html>

";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        echo "Enquestes";
    }

    // line 10
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    // line 50
    public function block_upperbar2($context, array $blocks = array())
    {
        // line 51
        echo "                <a href=\"/selectAllCustomers\">List</a>
                <a href=\"/insertCustomer\">Insert</a>
                <a href=\"/selectCustomer\">Select</a>
                <a href=\"/updateCustomer\">Update</a>
                <a href=\"/removeCustomer\">Remove</a>
                <br> <hr> <br>
            ";
    }

    // line 60
    public function block_body($context, array $blocks = array())
    {
    }

    // line 89
    public function block_scripts($context, array $blocks = array())
    {
    }

    // line 90
    public function block_javascripts($context, array $blocks = array())
    {
        // line 91
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-3.3.1.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t<script src=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/js.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  172 => 92,  167 => 91,  164 => 90,  159 => 89,  154 => 60,  144 => 51,  141 => 50,  136 => 10,  130 => 6,  123 => 94,  121 => 90,  119 => 89,  89 => 61,  87 => 60,  83 => 58,  81 => 50,  40 => 11,  38 => 10,  31 => 6,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base.html.twig", "/var/www/html/admin/app/Resources/views/base.html.twig");
    }
}
