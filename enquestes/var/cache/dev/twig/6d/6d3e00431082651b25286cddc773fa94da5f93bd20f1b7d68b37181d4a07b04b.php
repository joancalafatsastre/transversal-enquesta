<?php

/* product/content.html.twig */
class __TwigTemplate_464918c8ddb41bc48dc7ca1dafdd843aef0bb6ed82f3490487af8f90bf69ce18 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "product/content.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'bodys' => array($this, 'block_bodys'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "product/content.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "product/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div class=\"card-container\">
        ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["products"] ?? $this->getContext($context, "products")));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 6
            echo "        <div class=\"card\">
            <div class=\"card-image\">
                <figure class=\"image is-4by3\">
                    <img src=\"https://bulma.io/images/placeholders/1280x960.png\" alt=\"Placeholder image\">
                </figure>
            </div>
            <div class=\"card-content\">
                <div class=\"media\">
                    <div class=\"media-left\">
                        <span class=\"icon\">
                    ";
            // line 16
            if (($this->getAttribute($context["product"], "destacada", array()) == 1)) {
                // line 17
                echo "                        <i class=\"fas fa-star\"></i>
                    ";
            } else {
                // line 19
                echo "                        <i class=\"far fa-star\"></i>
                    ";
            }
            // line 21
            echo "                        </span>

                    </div>
                    <div class=\"media-content\">
                        <p class=\"title is-4\">ID: ";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "</p>
                        <p class=\"subtitle is-6\">";
            // line 26
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["product"], "dataInici", array()), "Y-m-d"), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["product"], "dataFinal", array()), "Y-m-d"), "html", null, true);
            echo "</p>

                    </div>
                </div>

                <div class=\"content\">
                    <p><span class=\"has-text-weight-bold\">Pregunta: </span>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "pregunta", array()), "html", null, true);
            echo "</p>

<p>Vots si:<span class=\"resultatsi\"></span>
\t\t<div class=\"votssi\">

\t\t";
            // line 37
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["info"] ?? $this->getContext($context, "info")));
            foreach ($context['_seq'] as $context["_key"] => $context["enquesta"]) {
                // line 38
                echo "\t\t\t\t
\t\t\t\t";
                // line 39
                if (($this->getAttribute($context["product"], "id", array()) == $this->getAttribute($context["enquesta"], "idEnquesta", array()))) {
                    // line 40
                    echo "\t\t\t\t    ";
                    if (($this->getAttribute($context["enquesta"], "valor", array()) == true)) {
                        // line 41
                        echo "<span>
\t\t\t\t\t\t";
                        // line 42
                        echo 0;
                        echo "
</span>
\t\t\t\t\t";
                    }
                    // line 45
                    echo "\t\t\t\t";
                }
                // line 46
                echo "\t\t\t\t
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enquesta'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo " 

\t\t</div>
\t\t
</p>


<p>Vots no: <span class=\"resultatno\"></span>
\t\t<div class=\"votsno\">

\t\t";
            // line 57
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["info"] ?? $this->getContext($context, "info")));
            foreach ($context['_seq'] as $context["_key"] => $context["enquesta"]) {
                // line 58
                echo "\t\t\t\t
\t\t\t\t";
                // line 59
                if (($this->getAttribute($context["product"], "id", array()) == $this->getAttribute($context["enquesta"], "idEnquesta", array()))) {
                    // line 60
                    echo "\t\t\t\t    ";
                    if (($this->getAttribute($context["enquesta"], "valor", array()) == false)) {
                        // line 61
                        echo "\t\t\t\t\t<span>
\t\t\t\t\t\t";
                        // line 62
                        echo 0;
                        echo "
\t\t\t\t\t</span>
\t\t\t\t\t";
                    }
                    // line 65
                    echo "\t\t\t\t";
                }
                // line 66
                echo "
\t\t\t\t
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enquesta'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 68
            echo " 

\t\t</div>
\t\t
</p>

                </div>
            </div>
        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 78
        echo "
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 82
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 83
        echo "    <style>
        .card-container {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-around;
        }

        .card {
            margin: 1rem;
            width: 30%;
        }
        .media-content {
            overflow: hidden;
        }
    </style>
";
        // line 99
        $this->displayBlock('bodys', $context, $blocks);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_bodys($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bodys"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bodys"));

        // line 100
        $this->displayBlock('javascripts', $context, $blocks);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 101
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-3.3.1.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t<script src=\"";
        // line 102
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/js.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "product/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  280 => 102,  275 => 101,  257 => 100,  239 => 99,  222 => 83,  213 => 82,  200 => 78,  185 => 68,  177 => 66,  174 => 65,  168 => 62,  165 => 61,  162 => 60,  160 => 59,  157 => 58,  153 => 57,  141 => 47,  134 => 46,  131 => 45,  125 => 42,  122 => 41,  119 => 40,  117 => 39,  114 => 38,  110 => 37,  102 => 32,  91 => 26,  87 => 25,  81 => 21,  77 => 19,  73 => 17,  71 => 16,  59 => 6,  55 => 5,  52 => 4,  43 => 3,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/product/content.html.twig #}
{% extends 'base.html.twig' %}
{% block body %}
    <div class=\"card-container\">
        {% for product in products %}
        <div class=\"card\">
            <div class=\"card-image\">
                <figure class=\"image is-4by3\">
                    <img src=\"https://bulma.io/images/placeholders/1280x960.png\" alt=\"Placeholder image\">
                </figure>
            </div>
            <div class=\"card-content\">
                <div class=\"media\">
                    <div class=\"media-left\">
                        <span class=\"icon\">
                    {% if product.destacada == 1 %}
                        <i class=\"fas fa-star\"></i>
                    {% else %}
                        <i class=\"far fa-star\"></i>
                    {% endif %}
                        </span>

                    </div>
                    <div class=\"media-content\">
                        <p class=\"title is-4\">ID: {{ product.id }}</p>
                        <p class=\"subtitle is-6\">{{ product.dataInici|date('Y-m-d') }} - {{ product.dataFinal|date('Y-m-d') }}</p>

                    </div>
                </div>

                <div class=\"content\">
                    <p><span class=\"has-text-weight-bold\">Pregunta: </span>{{ product.pregunta }}</p>

<p>Vots si:<span class=\"resultatsi\"></span>
\t\t<div class=\"votssi\">

\t\t{% for enquesta in info %}
\t\t\t\t
\t\t\t\t{%if product.id == enquesta.idEnquesta%}
\t\t\t\t    {%if enquesta.valor == true%}
<span>
\t\t\t\t\t\t{{0}}
</span>
\t\t\t\t\t{%endif%}
\t\t\t\t{%endif%}
\t\t\t\t
\t\t{% endfor %} 

\t\t</div>
\t\t
</p>


<p>Vots no: <span class=\"resultatno\"></span>
\t\t<div class=\"votsno\">

\t\t{% for enquesta in info %}
\t\t\t\t
\t\t\t\t{%if product.id == enquesta.idEnquesta%}
\t\t\t\t    {%if enquesta.valor == false%}
\t\t\t\t\t<span>
\t\t\t\t\t\t{{0}}
\t\t\t\t\t</span>
\t\t\t\t\t{%endif%}
\t\t\t\t{%endif%}

\t\t\t\t
\t\t{% endfor %} 

\t\t</div>
\t\t
</p>

                </div>
            </div>
        </div>
        {% endfor %}

    </div>

{% endblock %}
{% block stylesheets %}
    <style>
        .card-container {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-around;
        }

        .card {
            margin: 1rem;
            width: 30%;
        }
        .media-content {
            overflow: hidden;
        }
    </style>
{# Add javascript into end of the document (before any other javascript in the footer). #}
{% block bodys %}
{% block javascripts %}
    <script src=\"{{ asset('js/jquery-3.3.1.min.js') }}\" type=\"text/javascript\"></script>
\t<script src=\"{{ asset('js/js.js') }}\" type=\"text/javascript\"></script>
{% endblock %}
{% endblock %}
{% endblock %}
", "product/content.html.twig", "/var/www/html/app/Resources/views/product/content.html.twig");
    }
}
