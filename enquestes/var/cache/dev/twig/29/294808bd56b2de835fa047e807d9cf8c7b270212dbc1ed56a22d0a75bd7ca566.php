<?php

/* base.html.twig */
class __TwigTemplate_c3c6ed55db2ec552115354dd9cb3e0764cb978b1d540c4c4269e8ac20cd52387 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'upperbar2' => array($this, 'block_upperbar2'),
            'body' => array($this, 'block_body'),
            'scripts' => array($this, 'block_scripts'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 2
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css\">
        <script defer src=\"https://use.fontawesome.com/releases/v5.0.6/js/all.js\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js\"></script>
        ";
        // line 10
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
    <header>
        <nav class=\"navbar is-dark\" role=\"navigation\" aria-label=\"main navigation\">
            <div class=\"navbar-brand\">
                <a class=\"navbar-item\" href=\"/\">
                    <!--<img src=\"/img/polljtwox.png\" alt=\"Bulma: a modern CSS framework based on Flexbox\" width=\"112\" height=\"128\">
                </a>-->



                <div class=\"navbar-burger\" data-target=\"mainMenu\">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class=\"navbar-menu\" id=\"mainMenu\">
                <div class=\"navbar-end\">
                    <a href=\"/admin/selectAllProducts\" class=\"navbar-item\">
                        Llistar enquestes
                    </a>
                    <a href=\"/admin/insertProduct\" class=\"navbar-item\">
                        Afegir enquestes
                    </a>
                    
                    </a>
                    <a href=\"/admin/updateProduct\" class=\"navbar-item\">
                        Modificar enquestes
                    </a>
                    <a href=\"/admin/removeProduct\" class=\"navbar-item\">
                        Esborrar enquestes
                    </a>
                </div>
            </div>
        </nav>
    </header>
       <!-- <h3>Customer Management</h3>
        <div id=\"upperbar2\">
            ";
        // line 50
        $this->displayBlock('upperbar2', $context, $blocks);
        // line 58
        echo "        </div>-->
        <div id=\"content\">
            ";
        // line 60
        $this->displayBlock('body', $context, $blocks);
        // line 61
        echo "        </div>
    <script>
        document.addEventListener('DOMContentLoaded', function () {

            // Get all \"navbar-burger\" elements
            var \$navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

            // Check if there are any navbar burgers
            if (\$navbarBurgers.length > 0) {

                // Add a click event on each of them
                \$navbarBurgers.forEach(function (\$el) {
                    \$el.addEventListener('click', function () {

                        // Get the target from the \"data-target\" attribute
                        var target = \$el.dataset.target;
                        var \$target = document.getElementById(target);

                        // Toggle the class on both the \"navbar-burger\" and the \"navbar-menu\"
                        \$el.classList.toggle('is-active');
                        \$target.classList.toggle('is-active');

                    });
                });
            }

        });
    </script>
        ";
        // line 89
        $this->displayBlock('scripts', $context, $blocks);
        // line 90
        $this->displayBlock('javascripts', $context, $blocks);
        // line 94
        echo "    </body>
</html>

";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Enquestes";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 50
    public function block_upperbar2($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar2"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar2"));

        // line 51
        echo "                <a href=\"/selectAllCustomers\">List</a>
                <a href=\"/insertCustomer\">Insert</a>
                <a href=\"/selectCustomer\">Select</a>
                <a href=\"/updateCustomer\">Update</a>
                <a href=\"/removeCustomer\">Remove</a>
                <br> <hr> <br>
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 60
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 89
    public function block_scripts($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "scripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "scripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 90
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 91
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-3.3.1.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t<script src=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/js.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  250 => 92,  245 => 91,  236 => 90,  219 => 89,  202 => 60,  186 => 51,  177 => 50,  160 => 10,  142 => 6,  129 => 94,  127 => 90,  125 => 89,  95 => 61,  93 => 60,  89 => 58,  87 => 50,  46 => 11,  44 => 10,  37 => 6,  31 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/base.html.twig #}
<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>{% block title %}Enquestes{% endblock %}</title>
        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css\">
        <script defer src=\"https://use.fontawesome.com/releases/v5.0.6/js/all.js\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js\"></script>
        {% block stylesheets %}{% endblock %}
    </head>
    <body>
    <header>
        <nav class=\"navbar is-dark\" role=\"navigation\" aria-label=\"main navigation\">
            <div class=\"navbar-brand\">
                <a class=\"navbar-item\" href=\"/\">
                    <!--<img src=\"/img/polljtwox.png\" alt=\"Bulma: a modern CSS framework based on Flexbox\" width=\"112\" height=\"128\">
                </a>-->



                <div class=\"navbar-burger\" data-target=\"mainMenu\">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class=\"navbar-menu\" id=\"mainMenu\">
                <div class=\"navbar-end\">
                    <a href=\"/admin/selectAllProducts\" class=\"navbar-item\">
                        Llistar enquestes
                    </a>
                    <a href=\"/admin/insertProduct\" class=\"navbar-item\">
                        Afegir enquestes
                    </a>
                    
                    </a>
                    <a href=\"/admin/updateProduct\" class=\"navbar-item\">
                        Modificar enquestes
                    </a>
                    <a href=\"/admin/removeProduct\" class=\"navbar-item\">
                        Esborrar enquestes
                    </a>
                </div>
            </div>
        </nav>
    </header>
       <!-- <h3>Customer Management</h3>
        <div id=\"upperbar2\">
            {% block upperbar2 %}
                <a href=\"/selectAllCustomers\">List</a>
                <a href=\"/insertCustomer\">Insert</a>
                <a href=\"/selectCustomer\">Select</a>
                <a href=\"/updateCustomer\">Update</a>
                <a href=\"/removeCustomer\">Remove</a>
                <br> <hr> <br>
            {% endblock %}
        </div>-->
        <div id=\"content\">
            {% block body %}{% endblock %}
        </div>
    <script>
        document.addEventListener('DOMContentLoaded', function () {

            // Get all \"navbar-burger\" elements
            var \$navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

            // Check if there are any navbar burgers
            if (\$navbarBurgers.length > 0) {

                // Add a click event on each of them
                \$navbarBurgers.forEach(function (\$el) {
                    \$el.addEventListener('click', function () {

                        // Get the target from the \"data-target\" attribute
                        var target = \$el.dataset.target;
                        var \$target = document.getElementById(target);

                        // Toggle the class on both the \"navbar-burger\" and the \"navbar-menu\"
                        \$el.classList.toggle('is-active');
                        \$target.classList.toggle('is-active');

                    });
                });
            }

        });
    </script>
        {% block scripts %}{% endblock %}
{% block javascripts %}
    <script src=\"{{ asset('js/jquery-3.3.1.min.js') }}\" type=\"text/javascript\"></script>
\t<script src=\"{{ asset('js/js.js') }}\" type=\"text/javascript\"></script>
{% endblock %}
    </body>
</html>

", "base.html.twig", "/var/www/html/app/Resources/views/base.html.twig");
    }
}
