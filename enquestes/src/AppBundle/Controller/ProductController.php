<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Enquesta;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;


class ProductController extends Controller
{

    /**
     * @Route("/admin/insertProduct", name="insertProduct")
	 * @return array Retorna un array amb la plantilla twig 		renderizada amb el formulari
	 * @param $request Indica l'ID de l'enquesta per després 			mostrar el formulari que toca
     */

	
    public function insertProductAction(Request $request)
    {
        $product = new Enquesta();

        $form = $this->createFormBuilder($product)
            ->add('pregunta', TextType::class)
            ->add('data_inici', DateTimeType::class, array(
    'widget' => 'choice',
'years' => range(2018,2028),
'placeholder' => array(
        'year' => 'Any', 'month' => 'Mes', 'day' => 'Dia', 'hour'=>'Hores', 'minute'=>'Minuts'
    ),))
            ->add('data_final', DateTimeType::class, array(
    'widget' => 'choice',
'years' => range(2018,2028),
'placeholder' => array(
        'year' => 'Any', 'month' => 'Mes', 'day' => 'Dia', 'hour'=>'Hores', 'minute'=>'Minuts'
    ),))
			->add('destacada', NumberType::class)
            ->add('save', SubmitType::class, array('label' => 'Afegir enquesta'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();
            return $this->render('default/message.html.twig', array(
                'message' => 'Enquesta insertada: '. $product->getId()
            ));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Afegir Enquesta',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/selectAllProducts", 						name="selectAllProducts")
	 * @return array Retorna un array amb la plantilla twig 		renderizada amb totes les enquestes de la BBDD amb els 			seus vots a favor i en contra
     */
    public function selectAllProductsAction()
    {


$em = $this->getDoctrine()->getManager();
$array = $em->createQuery('SELECT u.id FROM AppBundle:Enquesta u')
            ->getResult();
$lastvalor= end($array);
$lastvalor = $lastvalor['id'];

for($count=1; $count<$lastvalor;$count++){


$users = $this->getDoctrine()
            ->getRepository('AppBundle:Resposta')
            ->findAll();
        if (count($users)==0) {
            return $this->render('default/message.html.twig', array(
                'message' => "No s'ha trobat cap enquesta"));
        }


$products = $this->getDoctrine()
            ->getRepository('AppBundle:Enquesta')
            ->findAll();
        if (count($products)==0) {
            return $this->render('default/message.html.twig', array(
                'message' => "No s'ha trobat cap enquesta"));
        }

        return $this->render('product/content.html.twig', array(
            'products' => $products, 'info' => $users));

}
        
    }

    /**
     * @Route("/admin/updateProduct", name="updateProduct")
	* @return array Retorna un array amb la plantilla twig 			renderizada amb el formulari
	 * @param $request Indica l'ID de l'enquesta per després 			mostrar el formulari que toca
     */
    public function updateProductAction(Request $request)
    {
        $product = new Enquesta();

        $form = $this->createFormBuilder($product)
            ->add('id', TextType::class, array('label' => "Posa l'ID de l'enquesta que vols modificar:"))
            ->add('save', SubmitType::class, array('label' => 'Update'))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $product = $em->getRepository('AppBundle:Enquesta')
                ->findOneById($product->getId());
            if (!$product) {
                return $this->render('default/message.html.twig', array(
                    'message' => "No s'ha trobat cap enquesta amb aquest ID"));
            }
print_r($product->getId());
            return $this->redirectToRoute('editProduct',array('name' => $product->getId()));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Update Enquesta',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/editProduct/{name}", name="editProduct")
	 * @return array Retorna un array amb la plantilla twig 		renderizada amb el formulari
	 * @param $request Indica l'ID de l'enquesta per després 			mostrar el formulari que toca
     */
    public function editProductAction($name, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('AppBundle:Enquesta')
           ->findOneById($name);

        $form = $this->createFormBuilder($product)
           
            ->add('pregunta', TextType::class)
            ->add('data_inici', DateTimeType::class)
            ->add('data_final', DateTimeType::class)
			->add('destacada', CheckboxType::class)
            ->add('save', SubmitType::class, array('label' => 'Update'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->render('default/message.html.twig', array(
                'message' => "Enquesta actualitzada"
            ));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Update Product',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/removeProduct", name="removeProduct")
	 * @return array Retorna un array amb la plantilla twig 		renderizada amb el formulari
	 * @param $request Indica l'ID de l'enquesta per després 			mostrar el formulari que toca
     */
    public function removeProductAction(Request $request)
    {
        $product = new Enquesta();

        $form = $this->createFormBuilder($product)
            ->add('id', NumberType::class, array('label' => "Posa l'ID de l'enquesta que vols eliminar:"))
            ->add('save', SubmitType::class, array('label' => 'Eliminar'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $product = $em->getRepository('AppBundle:Enquesta')
                      ->findOneById($product->getId());
            if (!$product) {
                return $this->render('default/message.html.twig', array(
                    'message' => "No s'ha trobat cap enquesta amb aquest ID"));
            }
            $id = $product->getId();
            $em->remove($product);
            $em->flush();
            return $this->render('default/message.html.twig', array(
                'message' => 'Enquesta eliminada: '. $id
            ));

        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Remove Product',
            'form' => $form->createView(),
        ));
    }

}
