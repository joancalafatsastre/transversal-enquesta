<?php

include_once 'sql_conn.php';

$status = '';
$content = $_POST['json'];
$content = json_decode($content, true);
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    $status = "Connection failed: " . $conn->connect_error;
}

$sql = "SELECT * FROM usuari WHERE email = '". $content['email'] ."' AND password = '". hash('sha256', $content['password']) ."' ";
$result = $conn->query($sql);
$row = $result->fetch_array(MYSQLI_ASSOC);

if (isset($_SESSION['id'])){
    $status = 'Ja estàs loguejat!';
} else {
    if ($row != null) {
        $status = "ok";
        session_start();
        $_SESSION['id'] = $row['id'];
    } else {
        $status = "Invalid login!";
    }
}


$conn->close();

$response = [
    "status" => $status,
    "data" => $row,
    "polls" => [
        "titulo" => "El Nesquik es mejor que el Cola Cao?",
        "id" => 23,
        "respondida" => false,
        "respuestas" => [
            "si" => 15434,
            "no" => 9424
        ]
    ]
];

echo json_encode($response);

