<?php
session_start();
$status = false;
if (isset($_SESSION['id'])) {
    $status = true;
}

$response = [
    "status" => $status,
    "id" => $_SESSION['id']
];
echo json_encode($response);