<?php

session_start();

$response = [
    "status" => session_destroy()
];

echo json_encode($response);