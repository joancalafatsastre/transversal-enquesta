$(function() {
	"use strict";

	// prova JSON

	let myObj = {
			"status": 1,
			"polls": [
				{
					"id": "1",
					"title": "adéu",
					"featured": 0,
					"start": "2019-01-01",
					"end": "2020-01-01",
					"answered": true
				},
				{
					"id": "2",
					"title": "Lydia te queremos",
					"featured": 1,
					"start": "2013-01-01",
					"end": "2013-01-01",
					"answered": false
				},
				{
					"id": "4",
					"title": "Hola Álvaro",
					"featured": 1,
					"start": "2018-06-07",
					"end": "2020-01-01",
					"answered": false
				}
			]
		};

	let myJSON = JSON.stringify(myObj);
	localStorage.setItem("testJSON", myJSON);

	let text = localStorage.getItem("testJSON");
	let obj = JSON.parse(text);
	console.log(obj);
	
	// JSON

	let data = {
		index: 0,
		url: ""
	};

	let model = {
		init: function() {
			//console.log('Modelo inicializado');

			/* navbar */
			$('nav a').on('click', function() {
				data.index = $(this).index();
				view.changeView();
			});
		},
		getJSON: function() {
			$.ajax({
				method: "GET",
				dataType: "json",
				url: "192.168.205.224"
			})
			.done(function(res) {
				console.log(res);
			})
			.fail(function() {
				// error
			})
		}
	};

	let controller = {
		//Al arrancar, lo que hacemos es iniciar el modelo y inicializar la vista
		init: function() {
			model.init();
			view.init();
		}
	};

	let view = {
		init: function() {
		//console.log('Modelo inicializado');
		},
		changeView: function() {
			switch(data.index) {
				case 0:
					data.url = "index.html";
					break;
				case 1:
					data.url = "web/llista.html";
					break;
				case 2:
					data.url = "web/resultats.html";
					break;
			}
			data.url += " .section";

			//$('#body').load(data.url);
			view.renderList();
		},
		renderList: function() {
			
			switch(data.index) {
				case 1:
					let template = $.ajax({type: "GET", url: "web/template.html", async: false}).responseText;
					let card;
					let html = `<section class="section"><div class="container"><div class="card-container">`;

					/* entra a polls */
					$.each(obj.polls, function(key, values) {

						html += `<div class="card">`;
						card = template;

						/* entra dins de cada grup en polls i agafa el nom dels camps i els valors */
						$.each(values, function(name, value) {
							let search = "{{ " + name + " }}";
							//if(card.match(search)){
								card = card.replace(search, value);
							//}
							
						});

						html += card + '</div>';
						//console.log(html);

					});
					html += '</div></div></div>';
					$('#body').html(html);

					break;
			}
		}
	};
	controller.init();

});