<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Enquesta;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;


class ProductController extends Controller
{

    /**
     * @Route("/admin/insertProduct", name="insertProduct")
     */
    public function insertProductAction(Request $request)
    {
        $product = new Enquesta();

        $form = $this->createFormBuilder($product)
            ->add('pregunta', TextType::class)
            ->add('data_inici', DateTimeType::class)
            ->add('data_final', DateTimeType::class)
			->add('destacada', NumberType::class)
            ->add('save', SubmitType::class, array('label' => 'Afegir enquesta'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();
            return $this->render('default/message.html.twig', array(
                'message' => 'Product inserted: '. $product->getId()
            ));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Afegir Enquesta',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/selectProduct", name="selectProduct")
     */
    public function selectProductAction(Request $request)
    {
        $product = new Enquesta();

        $form = $this->createFormBuilder($product)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Select'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $products = $this->getDoctrine()
                ->getRepository('AppBundle:Enquesta')
                ->findByName($product->getName());
            if (count($products)==0) {
                return $this->render('default/message.html.twig', array(
                    'message' => 'No products found'));
            }
            return $this->render('product/content.html.twig', array(
                'products' => $products));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Select Product',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/selectAllProducts", name="selectAllProducts")
     */
    public function selectAllProductsAction()
    {
        $products = $this->getDoctrine()
            ->getRepository('AppBundle:Enquesta')
            ->findAll();

        if (count($products)==0) {
            return $this->render('default/message.html.twig', array(
                'message' => 'No products found'));
        }
        return $this->render('product/content.html.twig', array(
            'products' => $products));
    }

    /**
     * @Route("/admin/selectProductsByCategory", name="selectProductsByCategory")
     */
    public function selectProductsByCategoryAction(Request $request)
    {

        $product = new Enquesta();

        $form = $this->createFormBuilder($product)
            ->add('category',EntityType::class, array('class' => 'AppBundle:Enquesta','choice_label' => 'id'))
            ->add('save', SubmitType::class, array('label' => 'Select'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /* $category = $this->getDoctrine()
                ->getRepository('AppBundle:Category')
                ->findOneByName($product->getCategory()->getName()); */

            $products = $product->getCategory()->getProducts();
            if (count($products)==0) {
                throw $this->createNotFoundException(
                    'No products found '
                );
            }
            return $this->render('product/content.html.twig', array(
                'products' => $products));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Select Product By Category',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/updateProduct", name="updateProduct")
     */
    public function updateProductAction(Request $request)
    {
        $product = new Enquesta();

        $form = $this->createFormBuilder($product)
            ->add('id', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Select'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $product = $em->getRepository('AppBundle:Enquesta')
                ->findOneById($product->getId());
            if (!$product) {
                return $this->render('default/message.html.twig', array(
                    'message' => 'No products found for name '. $product->getName()));
            }
            return $this->redirectToRoute('editProduct',array('id' => $product->getId()));

        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Update Enquesta',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/editProduct/{name}", name="editProduct")
     */
    public function editProductAction($name, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('AppBundle:Enquesta')
           ->findOneById($name);

        $form = $this->createFormBuilder($product)
            ->add('id', TextType::class)
            ->add('pregunta', TextType::class)
            ->add('data_inici', DateTimeType::class)
            ->add('data_final', DateTimeType::class)
			->add('destacada', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Update'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->render('default/message.html.twig', array(
                'message' => 'Product updated id: '. $product->getId()
            ));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Update Product',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/removeProduct", name="removeProduct")
     */
    public function removeProductAction(Request $request)
    {
        $product = new Enquesta();

        $form = $this->createFormBuilder($product)
            ->add('id', NumberType::class)
            ->add('save', SubmitType::class, array('label' => 'Remove'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $product = $em->getRepository('AppBundle:Enquesta')
                      ->findOneById($product->getId());
            if (!$product) {
                return $this->render('default/message.html.twig', array(
                    'message' => 'No products found for id '. $product->getId()));
            }
            $id = $product->getId();
            $em->remove($product);
            $em->flush();
            return $this->render('default/message.html.twig', array(
                'message' => 'Product deleted: '. $id
            ));

        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Remove Product',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/selectAllProductsOrderedByName", name="selectAllProductsOrderedByName")
     */
    public function selectAllProductsOrderedByNameAction()
    {
        $products = $this->getDoctrine()
            ->getRepository('AppBundle:Enquesta')
            ->findAllOrderedByName();

        if (count($products)==0) {
            return $this->render('default/message.html.twig', array(
                'message' => 'No products found'));
        }
        return $this->render('product/content.html.twig', array(
            'products' => $products));
    }
}
