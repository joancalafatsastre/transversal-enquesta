<?php
session_start();
$status = false;
if (isset($_SESSION['id'])) {
    $status = true;
}

$response = [
    "status" => $status,
    "id" => $_SESSION['id'],
	"admin" => intval($_SESSION['admin'])
];
echo json_encode($response);
