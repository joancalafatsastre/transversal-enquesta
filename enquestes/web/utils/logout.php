<?php
header('Access-Control-Allow-Origin: *');

session_start();

$response = [
    "status" => session_destroy()
];

echo json_encode($response);