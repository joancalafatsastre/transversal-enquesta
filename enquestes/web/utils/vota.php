<?php
header('Access-Control-Allow-Origin: *');

include_once 'sql_conn.php';

$content = $_POST['json'];
$content = json_decode($content, true);
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    $status = "Connection failed: " . $conn->connect_error;
}

$sql = "INSERT INTO resposta (id_usuari, id_enquesta, valor) VALUES ('" . $content['uid'] ."', '" . $content['pid'] ."', '" . $content['value'] ."')";
$result = $conn->query($sql);

echo json_encode($result);