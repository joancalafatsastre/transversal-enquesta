<?php

include_once 'sql_conn.php';

$status = '';
$content = $_POST['json'];
$content = json_decode($content, true);
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    $status = "Connection failed: " . $conn->connect_error;
}

$sql = "SELECT * FROM usuari WHERE email = '". $content['email'] ."' AND password = '". hash('sha256', $content['password']) ."' ";
$result = $conn->query($sql);
$row = $result->fetch_array(MYSQLI_ASSOC);

if (isset($_SESSION['id'])){
    $status = false;
} else {
    if ($row != null) {
        $status = true;
        session_start();
		
        $_SESSION['id'] = $row['id'];			
		$_SESSION['admin'] = $row['admin'];

    } else {
        $status = false;
    }
}


$conn->close();

$response = [
    "status" => $status,
    "data" => $row
];

echo json_encode($response);

