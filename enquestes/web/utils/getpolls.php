<?php

session_start();
 

include_once 'sql_conn.php';

$content = $_POST['json'];
$content = json_decode($content, true);
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    $status = "Connection failed: " . $conn->connect_error;
}

$sql = "SELECT * FROM enquesta";
// WHERE r.id_usuari = '" . $content['uid'] ."')"

$sql2 = "SELECT id_usuari, id_enquesta FROM resposta WHERE id_usuari = '". $content['uid'] ."'";





$result = $conn->query($sql);
$result2 = $conn->query($sql2);


$response = [
    "polls" => [

    ]
];

$answered_polls = [

];

while($row2 = $result2->fetch_assoc()) {
    array_push($answered_polls, $row2['id_enquesta']);
}

while($row = $result->fetch_assoc()) {
    $poll = [];
    $poll['id'] = $row['id'];
    $poll['title'] = utf8_encode($row['pregunta']);
    $poll['featured'] = intval($row['destacada']);
    $poll['start'] = $row['data_inici'];
    $poll['end'] = $row['data_final'];
    if (isset($_SESSION['id'])) {
        $poll['answered'] = in_array($row['id'], $answered_polls);
    }
    $no = "SELECT COUNT(valor) FROM resposta WHERE valor = 0 AND id_enquesta = '". $row['id'] ."'";
    $si = "SELECT COUNT(valor) FROM resposta WHERE valor = 1 AND id_enquesta = '". $row['id'] ."'";
    $sires = $conn->query($si);
    $nores = $conn->query($no);
    $sidata = $sires->fetch_array();
    $nodata = $nores->fetch_array();

    $poll['data'] = [
        "yes" => $sidata[0],
        "no" => $nodata[0]
    ];

    array_push($response['polls'], $poll);
};

echo json_encode($response);

