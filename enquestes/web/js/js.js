$( document ).ready(function() {
var votssi =0;
var votsno =0;

const cards = document.querySelectorAll('.card');

cards.forEach(e => {
	let si = e.querySelector('.resultatsi');
	let no = e.querySelector('.resultatno');

	votssi = e.querySelectorAll('.votssi > span').length;		
	votsno = e.querySelectorAll('.votsno > span').length;
	si.innerHTML = ` ${votssi}`;
	no.innerHTML = ` ${votsno}`;
})

$( ".votssi" ).hide();
$( ".votsno" ).hide();

$("label").wrapInner("<div class='field'></div>");
/*$("#form_data_inici").addClass("select is-primary is-small is-horizontal");
$("#form_data_final").addClass("select is-primary is-small is-horizontal");*/
$("button").addClass("button is-link");
$("input").addClass("input is-rounded is-primary is-small is-left");
$("label").addClass("label");
$("h3").addClass("subtitle is-2");

});
