// variables per comprovar posteriorment l'usuari
let user_id = null;
let is_admin = null;

// constatnts definides per a eleemnts de la pagina
const FORM = document.querySelector('#form');
const SESSION = document.querySelector('#session');
const CHECK = document.querySelector('#checkLogin');
const LOGOUT = document.querySelector('#logout');


function logOut() {
	fetch('web/utils/logout.php', {
		credentials: "same-origin"
	})
	.then(data => data.json())
	.then(res => {
		console.log(res);
		$('#llista, #logout').hide();
		$('#user-form').show();
		FORM.style.display = 'block';
		FORM.classList.remove('fadeOut');
		FORM.classList.add('fadeIn');
		//REGISTER.style.display = 'block';
		//REGISTER.classList.remove('fadeOut');
		//REGISTER.classList.add('fadeIn');

	})
}

function getPolls() {
	let payload = {
		uid: user_id
	};

	let data = new FormData();
	data.append( "json", JSON.stringify( payload ) );
	fetch("web/utils/getpolls.php",
	{
		method: "POST",
		body: data,
		credentials: "same-origin"
	})
	.then(res => res.json())
	.then(data => {
		//console.log(data);
		let votaJSON = JSON.stringify(data);
		localStorage.setItem("enquestesJSON", votaJSON);
	})
}

function vote(pid, value, pollNode) {
	let payload = {
		uid: user_id,
		pid: pid,
		value: value
	};

	let data = new FormData();
	data.append( "json", JSON.stringify( payload ) );
	let voteButton = pollNode.querySelector('.vote');
	voteButton.classList.add('is-loading');
	fetch("web/utils/vota.php",
	{
		method: "POST",
		body: data,
		credentials: "same-origin"
	})
	.then(res => res.json())
	.then(data => {
		voteButton.classList.remove('is-loading');
		console.log(data);
		if (data) {
			voteButton.setAttribute('disabled', 'true');
			voteButton.textContent = 'Gràcies per votar!'
		} else {
			voteButton.classList.add('is-danger');
			voteButton.setAttribute('disabled', 'true');
			voteButton.textContent = 'Ja has votat!'
		}
	})

}

function register(email, password) {
	let payload = {
		email: email,
		password: password
	};

	let data = new FormData();
	data.append( "json", JSON.stringify( payload ) );

	fetch("web/utils/register.php",
	{
		method: "POST",
		body: data,
		credentials: "same-origin"
	})
	.then(res => res.json())
	.then(data => console.log(data))
}

function checkLogin() {
	if (user_id != null) {
		return true;
	}
	return false;
}

function checkSession() {
	fetch('web/utils/session.php', {
		credentials: "same-origin"
	})
	.then(data => data.json())
	.then(res => {
		console.log(res);
		if (!res.status) {
			$('#llista, #logout').hide();
			$('#user-form').show();
		}
		user_id = res.id;
		is_admin = res.admin
		getPolls();
	})
}

function logIn(email, password) {
	let payload = {
		email: email,
		password: password
	};

	let data = new FormData();
	data.append( "json", JSON.stringify( payload ) );

	fetch("web/utils/validar_login.php",
	{
		method: "POST",
		body: data,
		credentials: "same-origin"
	})
	.then(function(res){ return res.json(); })
	.then(function(data){
		console.log(data);

		let message = document.createElement('div');
		message.setAttribute('id', 'loginMessage');
		message.classList.add('help');
		let messageText = '';

		if (data.status){
			messageText = document.createTextNode(`T'has loguejat correctament!`);
			message.classList.add('is-success');
			$('#llista, #logout').show();
			$('#user-form').hide();
			if (data.data.admin == 1) {
				location.href = "/admin/selectAllProducts";
			}
			/*
			setTimeout(function (e) {

			FORM.classList.add('fadeOut');
			REGISTER.classList.add('fadeOut');
			setTimeout(function (ee) {
			FORM.style.display = 'none';
			REGISTER.style.display = 'none'
			}, 1000)
			}, 1000)
			*/

		} else {
			messageText = document.createTextNode(`El correu o contrassenya no són vàlids!`);
			message.classList.add('is-danger');
		}
		message.appendChild(messageText);
		FORM.appendChild(message);
		//message.classList.add('animated');
		setTimeout(function (time) {
			//message.classList.add('fadeOut');
			setTimeout(function (tt) {
				FORM.removeChild(message);
			}, 1000)
		}, 2500);

	});
	checkSession();
}

$(function() {

	checkSession()

	/*
	FORM.classList.add('animated');
	FORM.addEventListener('submit', function (e) {
		e.preventDefault();
		let email = FORM.querySelector('input[name="email"]').value;
		let pass = FORM.querySelector('input[name="password"]').value;
		logIn(email, pass);
	});


	//REGISTER.classList.add('animated');

	REGISTER.addEventListener('submit', function (e) {
		e.preventDefault();
		let email = REGISTER.querySelector('input[name="email"]').value;
		let username = REGISTER.querySelector('input[name="username"]').value;
		let pass = REGISTER.querySelector('input[name="password"]').value;
		register(email, username, pass);
	});
	*/

	FORM.querySelector('#login').addEventListener('click', e => {
		let email = FORM.querySelector('input[name="email"]').value;
		let password = FORM.querySelector('input[name="password"]').value;
		logIn(email, password);
	})

	FORM.querySelector('#register').addEventListener('click', e => {
		let email = FORM.querySelector('input[name="email"]').value;
		let password = FORM.querySelector('input[name="password"]').value;
		register(email, password);
	})

	FORM.addEventListener('submit', e => {
		e.preventDefault();

		let email = FORM.querySelector('input[name="email"]').value;
		let password = FORM.querySelector('input[name="password"]').value;

		// logIn(email, password);
	})

	LOGOUT.addEventListener('click', e => logOut())

});
