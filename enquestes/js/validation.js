$(function() {
	
	// Validacio

	/* Funcio de delay */

	let delay = (function(){
		let timer = 0;
		return function(callback, ms){
			clearTimeout (timer);
			timer = setTimeout(callback, ms);
		};
	})();

	/* Expressions regulars */

	let exemail = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

	// Input

	// Email

	/* Keypress */

	$('input[type="email"]').on('keypress', function() {
		$('.validation').css('display', 'none');
		if (!$(this).parent().hasClass('is-loading')) {
			$(this).parent().addClass('is-loading');
		}
	});

	/* Keyup */

	$('input[type="email"]').on('keyup', function() {
		$('.validation').css('display', 'none');
		let email = $(this);
		email.removeClass('is-danger is-success');
		let value = email.val();
		delay(function() {
			email.parent().removeClass('is-loading');
			if(value.length > 0) {
				if(!email.val().match(exemail)){
					email.addClass('is-danger');
					$('.error').css('display', 'block');
					$('#login').prop("disabled", true);
				}
				else{
					email.addClass('is-success');
					$('.success').css('display', 'block');
					$('#login').prop("disabled", false);
				}
			}
		}, 300);
		
	});

	/* Focusout */

	$('input[name="email"]').focusout(function(){
		if(!$(this).val().match(exemail)){
			$('#login').prop("disabled", true);
		}
		else{
			$('#login').prop("disabled", false);
		}
	});
	
	// Password

	$('input[type="password"]').on('keyup', function() {
		$('.password-error').css('display', 'none');
		$('input[type="password"]').removeClass('is-danger');
	});

	/* Submit Click */

	$('#login').on('click', function(e) {
		if(!$('input[type="password"]').val() > 0) {
			e.preventDefault();
			$('.password-error').css('display', 'block');
			$('input[type="password"]').addClass('is-danger');
		}
	});

});