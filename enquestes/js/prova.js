$(function() {

	$(".fave, .not-fave").on("click", function() {
		let icon = $(this);
		icon.toggleClass("fave not-fave");
		icon.addClass("togglefave").delay(1000).queue(function(next) {
			icon.removeClass("togglefave");
			next();
		});
		console.log(icon.parent().next().children().get(0).innerText);
	});

	$('.navbar-item').on('click', function() {
		console.log($(this));
		$('.navbar-item').removeClass('current-nav');
		$(this).addClass('current-nav');
	});

	var ctx = document.getElementById("myChart").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'pie',
		data: {
			labels: ["No", "Yes"],
			datasets: [{
				label: '# of Votes',
				data: [12, 19],
				backgroundColor: [
					'rgba(255, 99, 132, 0.2)',
					'rgba(75, 192, 192, 0.2)'
				],
				borderColor: [
					'rgba(255,99,132,1)',
					'rgba(75, 192, 192, 1)'
				],
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				yAxes: [{
					display: false
				}],
				xAxes: [{
					display: false
				}]
			},
			legend: {
				display: false
			}
		}
	});
});