$(function() {
	"use strict";
	let POLLS;
	let data = {
		index: 0,
		obj: ""
	};

	let model = {
		init: function() {
			//console.log('Modelo inicializado');
			
			/* navbar */
			// canvia l'index en data amb l'opcio seleccionada i mostra la llista corresponent
			$('nav a').on('click', function() {
				data.index = $(this).index();
				view.renderList();
			});
			
			// invoca la funcio per obtenir el JSON
			model.getJSON();

		},
		getJSON: function() {
			// via ajax agafa el php que ens proporciona les dades i mostra la llista
			$.ajax({
			method: "GET",
			dataType: "json",
			url: "web/utils/getpolls.php"
			})
			.done(function(res) {
				let votaJSON = JSON.stringify(res);
				localStorage.setItem("enquestesJSON", votaJSON);
				view.renderList();
			})
			.fail(function() {
				console.log('Ha ocurregut un error inesperat.');
			})
		},
		updatePolls: function() {
			// actualitza les enquestes
			let text = localStorage.getItem("enquestesJSON");
			data.obj = JSON.parse(text);
		},
		checkVote: function() {
			// comprova si l'enquesta ha sigut resposta o no
			POLLS = document.querySelectorAll('.poll');

			POLLS.forEach(function (poll) {
				let pid = poll.id.slice(-1);
				let value = null;

				poll.querySelectorAll('.answers input').forEach(function (answer) {
					answer.addEventListener('click', function (e) {
						value = e.target.value;
						console.log(value);
					})
				});
				poll.querySelector('.vote').addEventListener('click', function (e) {
					if (value!= null) {
						vote(pid, value, poll);
					} else {
						alert('no has seleccionado ninguna opción')
					}
				});

			});
		}
	};

	let controller = {
		// inicia el model i la vista
		init: function() {
			model.init();
			view.init();
		}
	};

	let view = {
		init: function() {

			/* animacio de destacades */
			$(".fave, .not-fave").on("click", function() {
				let icon = $(this);

				/* intercanvia les classes entre destacada i no destacada */
				icon.toggleClass("fave not-fave");

				/* afageix la classe amb l'animacio corresponent i l'elimina al acabar */
				icon.addClass("togglefave").delay(1000).queue(function(next) {
					icon.removeClass("togglefave");
					next();
				});

				/* id de l'enquesta seleccionada */
				//console.log(icon.parent().next().children().get(0).innerText);
			});

			// canvia el navbar visualment per saber on ens situem
			$('.navbar-item').on('click', function() {
				$('.navbar-item').removeClass('current-nav');
				$(this).addClass('current-nav');
			});
		},
		renderList: function() {
			model.updatePolls();
			/* via ajax obte la template de les "cards" */
			let template = $.ajax({type: "GET", url: "web/template.html", async: false}).responseText;
			let card;
			let html = `<section class="section"><div class="container"><div class="card-container">`;

			switch(data.index) {
				case 0:
					/* entra a "polls" */
					$.each(data.obj.polls, function(key, values) {

						/* mostra nomes els "polls" marcats com a featured */
						if(values.featured) {

							if (values.answered) {
								html += `<div class="card poll answered" id="poll-${values.id}">`;
							} else {
								html += `<div class="card poll" id="poll-${values.id}">`;
							}
							card = template;
							card = card.replace("{{ icon }}", "fave");

							/* entra dins de cada grup en "polls" i agafa el nom dels camps i els valors */
							$.each(values, function(name, value) {
									let search = "{{ " + name + " }}";
									card = card.replace(search, value);
							});
						

							html += card + '</div>';
						}
					});
					html += '</div></div></div>';
					$('#body').html(html);

					model.checkVote();

					break;

				case 1:
					/* entra a "polls" */
					$.each(data.obj.polls, function(key, values) {

						/* mostra nomes els "polls" marcats com a featured */
							if (values.answered) {
								html += `<div class="card poll answered" id="poll-${values.id}">`;
							} else {
								html += `<div class="card poll" id="poll-${values.id}">`;
							}							card = template;
							if(values.featured == 1) {
							card = card.replace("{{ icon }}", "fave");
						}
						else {
							card = card.replace("{{ icon }}", "not-fave");
						}

							/* entra dins de cada grup en "polls" i agafa el nom dels camps i els valors */
							$.each(values, function(name, value) {
									let search = "{{ " + name + " }}";
									card = card.replace(search, value);
							});
						

							html += card + '</div>';
					});
					html += '</div></div></div>';
					$('#body').html(html);

					model.checkVote();

					break;
			}
			view.renderChart();
			view.init();
		},
		renderChart: function() {
			$.each($('canvas'), function(key) {

				/* agafem els vots per cada enquesta */
				let yesVote = data.obj.polls[key].data.yes;
				let noVote = data.obj.polls[key].data.no;

				/* funcio per a pintar el canvas amb Chartjs */
				let ctx = $('canvas')[key].getContext('2d');

				let myChart = new Chart(ctx, {
					type: 'pie',
					data: {
						labels: ["No", "Si"],
						datasets: [{
							label: '# of Votes',
							data: [noVote, yesVote],
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(75, 192, 192, 0.2)'
							],
							borderColor: [
								'rgba(255,99,132,1)',
								'rgba(75, 192, 192, 1)'
							],
							borderWidth: 1
						}]
					},
					options: {
						scales: {
							yAxes: [{
								display: false
							}],
							xAxes: [{
								display: false
							}]
						},
						legend: {
							display: false
						}
					}
				});
				

			});
		}
	};
	controller.init();

});
