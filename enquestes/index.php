<!DOCTYPE html>
<html lang="cat">
<head>
	<title>Enquestes</title>
	<meta charset="utf-8">
	<?php 

	?>

	<!-- Bulma + Fontawesome, jQuery -->

	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>

	<!--  -->

	<script type="text/javascript" src="js/validation.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	
</head>
<body>
	<header>
		<nav class="navbar" role="navigation" aria-label="main navigation">
			<div class="navbar-brand">
				<a class="navbar-item">
	      			<img id="logo" src="img/polljtwox.png" alt="Pàgina d'enquestes POLLJTWOX"  ">
			    </a>			</div>
			<div class="navbar-menu">
				<div class="navbar-start">
					<a class="navbar-item" id="home">
						Inici
					</a>
					<a class="navbar-item" id="llista">
						Llistar Enquestes
					</a>
					

				</div>

				<div class="navbar-end">
					<a class="navbar-item" id="logout">
						<i class="fa fa-sign-out-alt"></i>
					</a>
					<div class="navbar-item has-dropdown is-hoverable">
						<a class="navbar-link" id="user-form">
							<i class="fas fa-user"></i>
						</a>

						<div class="navbar-dropdown is-boxed">
							<form id="form" method="post">
								<label class="label">Login d'usuari</label>
								<hr class="navbar-divider">

								<!-- Input Email -->

								<div class="field">
									<div class="control has-icons-left has-icons-right">
										<input class="input" name="email" type="email" placeholder="Email">
										<span class="icon is-small is-left">
											<i class="fas fa-envelope"></i>
										</span>
										<span class="icon is-small is-right">
											<i class="fas fa-check validation success"></i>
											<i class="fas fa-exclamation-triangle validation error"></i>
										</span>
									</div>
									<p class="help is-danger validation error">This email is invalid</p>
								</div>

								<!-- Input Password -->

								<div class="field">
									<div class="control has-icons-left has-icons-right">
										<input class="input" name="password" type="password" placeholder="Password">
										<span class="icon is-small is-left">
											<i class="fas fa-lock"></i>
										</span>
										<span class="icon is-small is-right">
											<i class="fas fa-exclamation-triangle validation password-error"></i>
										</span>
									</div>
									<p class="help is-danger validation password-error">Enter a password</p>
								</div>
								<div class="field is-grouped">
									<div class="control">
										<button id="login" type="submit" class="button is-link" disabled>Sign In</button>
									</div>
									<div class="control">
										<button id="register" class="button">Sign Up</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</nav>
	</header>

	<div id="body">
		
	</div>

	<footer class="footer">
		<div class="container">
			<div class="content has-text-centered">
				<p>
					&#9400; 2018 Enquestes JxJ
				</p>
			</div>
		</div>
	</footer>
	<script type="text/javascript" src="js/frontend-functions.js"></script>
</body>
</html>
