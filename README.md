# Transversal enquesta

Joan Calafat | Xavier Paredes | Jerome Lomio

---

L'objectiu del projecte es crear una pàgina de gestió d'enquestes.

**Estat:** Disseny i planificació.

**Requeriments minims:**

* Crear enquestes per administrador.
* Autentificació d'usuari.
* Consultes binaries.
* L'usuari pot veure el seu vot dins la consulta.
* Grafic d'estadistiques amb els resultats obtinguts.
* Consultes identificades per URL.
* Portada de la pàgina.
* Navegació Single Page.
* Funcionalitats d'administrador.

---

[Full de Seguiment](https://docs.google.com/spreadsheets/d/1QkIp0XV6QeGgdl-RUvm2OWzuEOyBwM36gEVswpQuujU/edit?usp=sharing)

[Prototip del projecte](https://app.moqups.com/a15joacalsas@iam.cat/JrHBzn0wlU/view/page/a2493b428)

[Documentació PHPDoc]()

[Adreça web del labs]()
